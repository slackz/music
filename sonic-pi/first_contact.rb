QUARTER   = 0.8
HALF      = QUARTER * 2
WHOLE     = QUARTER * 4
SIXTEENTH = QUARTER / 4
EIGHTH    = QUARTER / 2

def sync_sample(s_name, sleep_dur = QUARTER)
  live_loop s_name do
    sync :melody
    sample s_name
    sleep sleep_dur
  end
end

sync_sample(:ambi_lunar_land, WHOLE)
sync_sample(:ambi_drone, WHOLE)
sync_sample(:drum_bass_hard, WHOLE)
sync_sample(:bass_woodsy_c, HALF)
sync_sample(:drum_snare_soft, SIXTEENTH)
sync_sample(:drum_cymbal_closed, QUARTER)

live_loop :melody do
  synths = [:fm, :dsaw, :prophet]
  use_synth :fm
  play scale(:c4, :minor_pentatonic).sample
  sleep SIXTEENTH
end
