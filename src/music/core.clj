(ns music.core
  (:use [overtone.live])
  (:require [music.samples    :as samples]
            [music.midi       :as midi]
            [music.percussion :as percussion]))

(def samples-hash {36 :kate-bush 38 :snare-roll-1 40 :snare-roll-2 41 :snare-roll-3})
(midi/note-event samples-hash)

;; (percussion/rhythm)
;; (stop)
