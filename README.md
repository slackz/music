# music

Playing around with samples, drum tracks, synthesizers, etc

## License

Copyright © 2015 Christopher Kuttruff

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
